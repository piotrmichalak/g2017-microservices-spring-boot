## Gathering 2017 ##
## Implementing Microservices with Java and Spring Boot ##

## Agenda ##
1. Goals
2. What are microservices?
3. Introduction to Spring Boot
4. Configuration management "Twelve-Factor App“
5. Spring Data
6. Spring REST
7. Spring Boot Actuator
8. Spring Cloud
9. Spring Cloud Config
10. Service Registration and Discovery with Eureka
11. Edge Services with Zuul


### Goals ###
![Goals](/Goals.png)


##### What are Microservices #####
[Microservices](https://www.martinfowler.com/microservices/)  

_In short, the microservice architectural style is an approach to developing a single application as a suite of small services, each running in its own process and communicating with lightweight mechanisms, often an HTTP resource API. These services are built around business capabilities and independently deployable by fully automated deployment machinery. There is a bare minimum of centralized management of these services, which may be written in different programming languages and use different data storage technologies._  

_-- James Lewis and Martin Fowler_




###### Monolits and Microservices ######
![MonolithsAndMicroservices](/sketch.png)

###### Conway's Law ######

_Any organization that designs a system (defined broadly) will produce a design whose structure is a copy of the organization's communication structure._

_-- Melvyn Conway, 1967_

![ConwaysLaw](/conways-law.png)

###### Service boundaries reinforced by team boundaries ######

![Boundaries](/PreferFunctionalStaffOrganization.png)

###### Decentralized Data ######
![DecentralizedData](/decentralised-data.png)

###### Deployment  ######
![Deployment](/micro-deployment.png)

##### Spring Boot #####
[Spring Boot](http://projects.spring.io/spring-boot/)  

##### Twelwe-Factor App #####
[Twelwe-Factor App](https://12factor.net/)  

##### Spring data #####

[Spring Data](http://docs.spring.io/spring-data/jpa/docs/current/reference/html/)  
[Query Methods](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.query-methods.details)
[Spring Data REST](http://projects.spring.io/spring-data-rest/)

##### Spring REST #####

[Spring HATEOS](http://projects.spring.io/spring-hateoas/)  
[Spring Restdocs](http://projects.spring.io/spring-restdocs/)  

##### Spring Boot Actuator #####

[Actuator](https://github.com/spring-projects/spring-boot/tree/master/spring-boot-actuator)  

* /health – Shows application health information (a simple ‘status’ when accessed over an unauthenticated connection or full message details when authenticated). It is not sensitive by default.
* /info – Displays arbitrary application info. Not sensitive by default.
* /metrics – Shows ‘metrics’ information for the current application. It is also sensitive by default.
* /trace – Displays trace information (by default the last few HTTP requests).

##### Spring Cloud #####
[Spring Cloud](http://projects.spring.io/spring-cloud/)  

##### Spring Cloud Config #####
[Spring Cloud Config](https://cloud.spring.io/spring-cloud-config/)  

##### Eureka #####

[Eureka 1](https://spring.io/blog/2015/01/20/microservice-registration-and-discovery-with-spring-cloud-and-netflix-s-eureka)  

[Eureka 2](https://github.com/Netflix/eureka/wiki/Eureka-at-a-glance)  

##### Zuul #####

[Zuul](https://github.com/Netflix/zuul/wiki)  

[Hystrix](https://github.com/Netflix/Hystrix/wiki)  



##### Spring Initializr #####

[SPRING INITIALIZR](http://start.spring.io)



##### Beans #####

* InitializingBean
* CommandLineRunner

### Service1 - REST + JPA ###

##### Dependencies #####

* Web
* JPA
* h2
* Actuator
* Rest Repositories
* Config Client
* Eureka Discovery


##### application.properties #####

server.port=8001  
spring.application.name=service1  
security.basic.enabled=false  

##### bootstrap.properties #####

spring.application.name=service1  
spring.cloud.config.uri=http://localhost:8888  
security.basic.enabled=false  
endpoints.sensitive=false  
spring.cloud.config.fail-fast=true  


##### Classes #####

* Entity
* Repository
* RestController

##### Annotations  #####

* '@'Entity
* '@'Id
* @GeneratedValue
* @RestController
* @EnableDiscoveryClient
* @RepositoryRestResource



### Service2 - REST + MongoDB ###

##### Dependencies #####

* Web
* MongoDB
* Embedded MongoDB
* Actuator
* Rest Repositories
* Config Client
* Eureka Discovery


##### application.properties #####

server.port=9002  
spring.application.name=service2  
security.basic.enabled=false  

### Config Server ###

##### Dependencies #####

* Config Server

##### Git repository #####
* Go to ${user.home}
* mkdir config-repo
* git init
* create file service1.properties
* create file service2.properties

##### application.properties #####
server.port=8888  
spring.cloud.config.server.git.uri=file://${user.home}/config-repo  
endpoints.configprops.sensitive=false  

##### bootstrap.properties #####
spring.application.name=config-server

##### Classes #####

* Document
* Repository
* RestController

##### Annotations  #####

* @Document
* '@'Id
* @RestController
* @EnableDiscoveryClient

##### Links #####
[service1 master](http://localhost:8888/service1/master)


### Eureka Server ###

##### Dependencies #####

* Eureka Server


##### application.properties #####

spring.application.name=eureka-server  
server.port=8761  
eureka.client.fetch-registry=false  
eureka.client.register-with-eureka=false  

##### Annotations  #####
* @EnableEurekaServer

### Zuul Server ###

##### Dependencies #####

* Zuul


##### application.properties #####

spring.application.name=zuul  
server.port=9090  
endpoints.sensitive=false  
zuul.prefix=/api  
zuul.routes.test.path=/redirect/\*\*  
zuul.routes.test.url=http://www.google.com  
zuul.routes.service1-by-service.path=/service1-by-servicve/\*\*  
zuul.routes.service1-by-service.serviceId=SERVICE1  
zuul.routes.service1-by-address.path=/service1-by-address/\*\*    
zuul.routes.service1-by-address.url=http://localhost:8001  
eureka.client.service-url.defaultZone=http://127.0.0.1:8761/eureka/  
